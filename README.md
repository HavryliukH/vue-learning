# vue-lerning

Project to learning of Vue.js for Hillel Evo.

## Getting Started

These instructions will get you a copy of the project up and run on your local machine for development.

### Prerequisites

You need to **node.js** and **npm** for run project on your local machine - https://nodejs.org/

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Authors

* **Nick Gromov** - *Initial work* - [NickGromov92](https://gitlab.com/NickGromov92)
