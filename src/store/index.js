import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		isLogin: false,
		name: '',
	},

	mutations: {
		setAuthState (state, authState) {
			state.isLogin = authState;
		},
		setName (state, name) {
			state.name = name;
		}
	}
})
