import Vue from 'vue';
import Router from 'vue-router';
import Auth from '@/components/Auth.vue';
import Content from '@/components/Content.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/auth',
			component: Auth

		},
		{
			path: '/content',
			component: Content,
		}
	]
})
